<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- Datatable Scripts And Styles -->
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <link href="../dist/jsoneditor.min.css" rel="stylesheet" type="text/css">
    <script src="../dist/jsoneditor.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h3>Add New Record</h3>
        <div id="jsoneditor" style="width: 400px; height: 400px;"></div>
        <br>
        <a href="/game" class="btn btn-warning">Go Back</a>
        <input type="submit" value="Submit" onclick="getJSON();" class="btn btn-primary">
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script>

    // create the editor
    var container = document.getElementById("jsoneditor");
    var options = {};

    var editor = new JSONEditor(container, options);

    var errors = editor.validate({
        value: {
            to: "test"
        }
    });

    // set json
    var json = {
        "GameName": "", "WinRate": "", "Credits": ""
    };
    editor.set(json);

    // get json
    function getJSON() {
        var json = editor.get();
        var flagSet = 0;
        if (json.Credits == '') {
            flagSet = 1;
        }
        if (json.GameName == '')
        {
            flagSet = 1;
        }
        if (json.WinRate == '')
        {
            flagSet = 1;
        }
        if (flagSet == 0) {
            $.ajax({
                type: 'POST',
                url: "/game",
                data: json,
                dataType: "text",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (resultData) {
                        swal({
                            title: "It worked!",
                            text: "Record Created Successfully",
                            type: "success",
                            showConfirmButton: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                window.location.href = "/game";
                            }
                        });
                }
            });
        }else{
            swal({
                title: "Error in Data!",
                text: "All Fields Are Compulsory",
                type: "error",
                showConfirmButton: false
            });
        }
    }
</script>
</body>
</html>
