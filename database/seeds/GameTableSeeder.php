<?php

use Illuminate\Database\Seeder;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('game')->insert([
            'game_details' => '{"Game_Name":"Assasins Creed","Win_Rate":"25","Credits":"797"}',
        ]);
    }
}
