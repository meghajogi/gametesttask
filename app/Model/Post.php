<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'game';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
